# Keycloak Client

**Keycloak Clienty Legacy IS** extends the functionnalities provided by the `keycloak-client` library adding the endpoints discovery functions against the legacy Information System based on context's scope. For this reason the compatibility is with Smart Gears v.3 and not with SGv4 or newer.

## Structure of the project

The source code is present in `src` folder. 

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

## Documentation

To build the library JAR it is sufficient to type

    mvn clean package

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/authorization-client/releases).

## Authors

* **Mauro Mugnaini** ([Nubisware S.r.l.](http://www.nubisware.com))

## How to Cite this Software
[Intentionally left blank]

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.

## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

## Acknowledgments
[Intentionally left blank]