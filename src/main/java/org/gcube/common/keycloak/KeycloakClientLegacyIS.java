package org.gcube.common.keycloak;

import java.net.URL;
import java.util.List;

import org.gcube.common.keycloak.model.TokenIntrospectionResponse;
import org.gcube.common.keycloak.model.TokenResponse;
import org.gcube.common.scope.api.ScopeProvider;

@SuppressWarnings("deprecation")
public interface KeycloakClientLegacyIS extends KeycloakClient {

    String CATEGORY = "Auth";
    String NAME = "IAM";
    String DESCRIPTION = "oidc-token endpoint";

    /**
     * Finds the keycloak <code>token</code> endpoint {@link URL} discovering it in the current scope provided by {@link ScopeProvider}
     * 
     * @return the keycloak <code>token</code> endpoint URL in the current scope
     * @throws KeycloakClientException if something goes wrong discovering the endpoint URL
     */
    URL findTokenEndpointURL() throws KeycloakClientException;

    /**
     * Compute the keycloak <code>introspection</code> endpoint {@link URL} starting from the discovered token endpoint it in the current scope provided by {@link ScopeProvider}.
     * 
     * @return the keycloak <code>introspection</code> endpoint URL in the current scope
     * @throws KeycloakClientException if something goes wrong discovering the endpoint URL
     */
    URL computeIntrospectionEndpointURL() throws KeycloakClientException;

    /**
     * Queries an OIDC token from the Keycloak server discovered in the current scope, by using provided clientId and client secret.
     * 
     * @param clientId the client id
     * @param clientSecret the client secret
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryOIDCToken(String clientId, String clientSecret) throws KeycloakClientException;

    /**
     * Queries an UMA token from the Keycloak server discovered in the current scope, by using access-token provided by the {@link TokenResponse} object
     * for the given audience (context), in URLEncoded form or not, and optionally a list of permissions.
     * 
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param audience the audience (context) where to request the issuing of the ticket
     * @param permissions a list of permissions, can be <code>null</code>
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryUMAToken(TokenResponse oidcTokenResponse, String audience, List<String> permissions)
            throws KeycloakClientException;

    /**
     * Queries an UMA token from the Keycloak server discovered in the current scope, by using provided clientId and client secret
     * for the given audience (context), in URLEncoded form or not, and optionally a list of permissions.
     * 
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param audience the audience (context) where to request the issuing of the ticket
     * @param permissions a list of permissions, can be <code>null</code>
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryUMAToken(String clientId, String clientSecret, String audience, List<String> permissions)
            throws KeycloakClientException;

    /**
     * Queries an UMA token from the Keycloak server discovered in the current scope, by using provided clientId and client secret
     * for the current scope as audience (context), in URLEncoded form or not, and optionally a list of permissions.
     * 
     * @param clientId the client id
     * @param clientSecret the client secret
     * @param permissions a list of permissions, can be <code>null</code>
     * @return the issued token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the query
     */
    TokenResponse queryUMAToken(String clientId, String clientSecret, List<String> permissions)
            throws KeycloakClientException;

    /**
     * Refreshes a previously issued token from the Keycloak server discovered in the current scope using the refresh
     * token JWT encoded string in the token response object.
     * 
     * Client id will be read from "issued for" access token's claim and client secret will be not sent.
     * <br><b>NOTE</b>: For <code>public</code> clients types only.
     * 
     * @param tokenResponse the previously issued token as {@link TokenResponse} object
     * @return the refreshed token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the refresh query
     */
    TokenResponse refreshToken(TokenResponse tokenResponse) throws KeycloakClientException;

    /**
     * Refreshes a previously issued token from the Keycloak server discovered in the current scope using the refresh
     * token JWT encoded string in the token response object and the provided client id.
     * 
     * Client secret will be not sent.
     * <br><b>NOTE</b>: For <code>public</code> clients types only.
     * 
     * @param clientId the requestor client id, may be <code>null</code> and in this case will be take from the access token "issued for" claim
     * @param tokenResponse the previously issued token as {@link TokenResponse} object
     * @return the refreshed token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the refresh query
     */
    TokenResponse refreshToken(String clientId, TokenResponse tokenResponse) throws KeycloakClientException;

    /**
     * Refreshes a previously issued token from the Keycloak server discovered in the current scope using the refresh
     * token JWT encoded string in the token response object and the provided client id and secret.
     * 
     * @param clientId the requestor client id, may be <code>null</code> and in this case will be take from the access token "issued for" claim
     * @param clientSecret the requestor client secret, may be <code>null</code> for non-confidential clients
     * @param tokenResponse the previously issued token as {@link TokenResponse} object
     * @return the refreshed token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the refresh query
     */
    TokenResponse refreshToken(String clientId, String clientSecret, TokenResponse tokenResponse)
            throws KeycloakClientException;

    /**
     * Refreshes a previously issued token from the Keycloak server discovered in the current scope using the  the refresh token JWT encoded string obtained with the access token in the previous token response.
     * 
     * Client id will be read from "issued for" refresh token's claim and client secret will be not sent.
     * <br><b>NOTE</b>: For <code>public</code> clients types only.
     * 
     * @param refreshTokenJWTString the previously issued refresh token JWT string taken from the same token response of the access token parameter
     * @return the refreshed token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the refresh query
     */
    TokenResponse refreshToken(String refreshTokenJWTString) throws KeycloakClientException;

    /**
     * Refreshes a previously issued token from the Keycloak server discovered in the current scope using the provided
     * client id and the refresh token JWT encoded string obtained with the access token in the previous token response.
     * 
     * Client secret will be not used.
     * <br><b>NOTE</b>: For <code>public</code> clients types only.
     * 
     * @param clientId the requestor client id
     * @param refreshTokenJWTString the previously issued refresh token JWT string taken from the same token response of the access token parameter
     * @return the refreshed token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the refresh query
     */
    TokenResponse refreshToken(String clientId, String refreshTokenJWTString) throws KeycloakClientException;

    /**
     * Refreshes a previously issued token from the Keycloak server discovered in the current scope using the provided
     * client id and secret and the refresh token JWT encoded string obtained with the access token in the previous
     * token response.
     * 
     * @param clientId the requestor client id
     * @param clientSecret the requestor client secret, may be <code>null</code> for non-confidential clients
     * @param refreshTokenJWTString the previously issued refresh token JWT string taken from the same token response of the access token parameter
     * @return the refreshed token as {@link TokenResponse} object
     * @throws KeycloakClientException if something goes wrong performing the refresh query
     */
    TokenResponse refreshToken(String clientId, String clientSecret, String refreshTokenJWTString)
            throws KeycloakClientException;

    /**
     * Introspects an access token against the Keycloak server discovered in the current scope.
     * 
     * @param clientId the requestor client id
     * @param clientSecret the requestor client secret
     * @param accessTokenJWTString the access token to verify
     * @return <code>true</code> if the token is valid, <code>false</code> otherwise
     * @throws KeycloakClientException if something goes wrong performing the verification
     */
    TokenIntrospectionResponse introspectAccessToken(String clientId, String clientSecret, String accessTokenJWTString) throws KeycloakClientException;

    /**
     * Verifies an access token against the Keycloak server discovered in the current scope.
     * 
     * @param clientId the requestor client id
     * @param clientSecret the requestor client secret
     * @param accessTokenJWTString the access token to verify
     * @return a {@link TokenIntrospectionResponse} object with the introspection results; in particular, the <code>active</code> field represents the token validity
     * @throws KeycloakClientException if something goes wrong performing the verification
     */
    boolean isAccessTokenVerified(String clientId, String clientSecret, String accessTokenJWTString) throws KeycloakClientException;

}