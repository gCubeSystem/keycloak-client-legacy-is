package org.gcube.common.keycloak;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KeycloakClientLegacyISFactory {

    protected static final Logger logger = LoggerFactory.getLogger(KeycloakClientLegacyISFactory.class);

    public static KeycloakClientLegacyIS newInstance() {
        logger.debug("Instantiating a new keycloak client for legacy IS instance");
        return new DefaultKeycloakClientLegacyIS();
    }
 
}