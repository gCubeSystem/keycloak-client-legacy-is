package org.gcube.common.keycloak;

import java.net.URL;

import org.gcube.common.keycloak.model.ModelUtils;
import org.gcube.common.keycloak.model.TokenIntrospectionResponse;
import org.gcube.common.keycloak.model.TokenResponse;
import org.gcube.common.scope.api.ScopeProvider;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("deprecation")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestKeycloakClientLegacyIS {

    protected static final Logger logger = LoggerFactory.getLogger(TestKeycloakClientLegacyIS.class);

    protected static final String DEV_TOKEN_ENDPOINT = "https://accounts.dev.d4science.org/auth/realms/d4science/protocol/openid-connect/token";
    protected static final String DEV_INTROSPECTION_ENDPOINT = DEV_TOKEN_ENDPOINT + "/introspect";

    protected static final String CLIENT_ID = "keycloak-client-unit-test";
    protected static final String CLIENT_SECRET = "ebf6f82e-9511-408e-8321-203081e472d8";
    protected static final String TEST_AUDIENCE = "conductor-server";
    protected static final String OLD_OIDC_ACCESS_TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJSSklZNEpoNF9qdDdvNmREY0NlUDFfS1l0akcxVExXVW9oMkQ2Tzk1bFNBIn0.eyJleHAiOjE2NTI5Nzk4NDUsImlhdCI6MTY1Mjk3OTU0NSwianRpIjoiMzQ2MjgwMWItODg4NS00YTM4LWJkNDUtNWExM2U1MGE5MGU5IiwiaXNzIjoiaHR0cHM6Ly9hY2NvdW50cy5kZXYuZDRzY2llbmNlLm9yZy9hdXRoL3JlYWxtcy9kNHNjaWVuY2UiLCJhdWQiOlsiJTJGZ2N1YmUiLCIlMkZnY3ViZSUyRmRldnNlYyUyRmRldlZSRSIsImFjY291bnQiXSwic3ViIjoiYTQ3ZGZlMTYtYjRlZC00NGVkLWExZDktOTdlY2Q1MDQzNjBjIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoia2V5Y2xvYWstY2xpZW50Iiwic2Vzc2lvbl9zdGF0ZSI6ImQ4MDk3MDBmLWEyNDUtNDI3Zi1hYzhjLTQxYjFkZDNkYTQ3MCIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsIkluZnJhc3RydWN0dXJlLUNsaWVudCIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiJTJGZ2N1YmUiOnsicm9sZXMiOlsiTWVtYmVyIl19LCJrZXljbG9hay1jbGllbnQiOnsicm9sZXMiOlsidW1hX3Byb3RlY3Rpb24iXX0sIiUyRmdjdWJlJTJGZGV2c2VjJTJGZGV2VlJFIjp7InJvbGVzIjpbIk1lbWJlciJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiY2xpZW50SWQiOiJrZXljbG9hay1jbGllbnQiLCJjbGllbnRIb3N0IjoiOTMuNjYuMTg1Ljc1IiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJzZXJ2aWNlLWFjY291bnQta2V5Y2xvYWstY2xpZW50IiwiY2xpZW50QWRkcmVzcyI6IjkzLjY2LjE4NS43NSJ9.FQu4ox2HWeqeaY7nHYVGeJVpkJOcASfOb8tbOUeG-GB6sMjRB2S8PjLLaw63r_c42yxKszP04XdxGqIWqXTtoD9QCiUHTT5yJTkIpio4tMMGHth9Fbx-9dwk0yy_IFi1_OsCvZFmOQRdjMuUkj1lSqslCzAw-2E5q1Zt415-au5pEVJYNTFqIsG72ChJwh6eq1Dh1XBy8krb7YVPQyIwxO_awgAYO5hbsdvXYlRfCrnB38kk2V6-CQ-XYoL1m7xIB-gjhKCiFvDmmntQSRCZFgb0qi8eOmh9FdzPxZgx7yPJwAAj17dS4B_gz9FpZBVciNzpA6Lf4P2bqvoD9-R6ow";
    protected static final String OLD_UMA_ACCESS_TOKEN = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJSSklZNEpoNF9qdDdvNmREY0NlUDFfS1l0akcxVExXVW9oMkQ2Tzk1bFNBIn0.eyJleHAiOjE2NTI5ODA0NzgsImlhdCI6MTY1Mjk4MDE3OCwianRpIjoiNjBkNzU3MGMtZmQxOC00NGQ1LTg1MzUtODhlMmFmOGQ1ZTgwIiwiaXNzIjoiaHR0cHM6Ly9hY2NvdW50cy5kZXYuZDRzY2llbmNlLm9yZy9hdXRoL3JlYWxtcy9kNHNjaWVuY2UiLCJhdWQiOiJjb25kdWN0b3Itc2VydmVyIiwic3ViIjoiYTQ3ZGZlMTYtYjRlZC00NGVkLWExZDktOTdlY2Q1MDQzNjBjIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoia2V5Y2xvYWstY2xpZW50Iiwic2Vzc2lvbl9zdGF0ZSI6IjI3NDUyN2M5LWNkZjMtNGM2Yi1iNTUxLTFmMTRkZGE5ZGVlZiIsImFjciI6IjEiLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiSW5mcmFzdHJ1Y3R1cmUtQ2xpZW50Il19LCJhdXRob3JpemF0aW9uIjp7InBlcm1pc3Npb25zIjpbeyJzY29wZXMiOlsiZ2V0Il0sInJzaWQiOiIyNDlmZDQ2OS03OWM1LTRiODUtYjE5NS1mMjliM2ViNjAzNDUiLCJyc25hbWUiOiJtZXRhZGF0YSJ9LHsic2NvcGVzIjpbImdldCIsInN0YXJ0IiwidGVybWluYXRlIl0sInJzaWQiOiJhNmYzZWFkZS03NDA0LTRlNWQtOTA3MC04MDBhZGI1YWFjNGUiLCJyc25hbWUiOiJ3b3JrZmxvdyJ9LHsicnNpZCI6IjFiNmMwMGI3LTkxMzktNGVhYS1hYWM3LTIwMjMxZmVlMDVhNSIsInJzbmFtZSI6IkRlZmF1bHQgUmVzb3VyY2UifV19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJjbGllbnRJZCI6ImtleWNsb2FrLWNsaWVudCIsImNsaWVudEhvc3QiOiI5My42Ni4xODUuNzUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInByZWZlcnJlZF91c2VybmFtZSI6InNlcnZpY2UtYWNjb3VudC1rZXljbG9hay1jbGllbnQiLCJjbGllbnRBZGRyZXNzIjoiOTMuNjYuMTg1Ljc1In0.Hh62E56R-amHwoDPFQEylMvrvmNzWnC_4bDI7_iQYAPJ5YzCNH9d7zcdGaQ96kRmps_JRc2Giv_1W9kYorOhlXl-5QLDrSoqrqFxrNpEGG5r5jpNJbusbu4wNUKiCt_GMnM1UmztgXiQeuggNGkmeBIjotj0eubnmIbUV9ukHj3v7Z5PwNKKX3BCpsghd1u8lg6Nfqk_Oho4GXUfdaFY_AR3SNqzVI_9YLhND_a03MNNWlnfOvj8T4nDCKBZIs91tVyiu98d2TjnQt8PdlVwokMP3LA58m0Khy2cmUm1KF2k0zlzP8MxV9wTxNrpovMr-PnbtEPZ_IlVQIzHwjHfwQ";

    private static TokenResponse oidcTR = null;
    private static TokenResponse umaTR = null;

    @Before
    public void setUp() throws Exception {
        ScopeProvider.instance.set("/gcube");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test00TokenEndpointDiscovery() throws Exception {
        logger.info("*** [0.0] Start testing Keycloak token endpoint discovery...");
        URL url = KeycloakClientLegacyISFactory.newInstance().findTokenEndpointURL();
        Assert.assertNotNull(url);
        Assert.assertTrue(url.getProtocol().equals("https"));
        Assert.assertEquals(new URL(DEV_TOKEN_ENDPOINT), url);
        logger.info("Discovered URL is: {}", url);
    }

    @Test
    public void test01IntrospectEndpointComputeFromDiscovered() throws Exception {
        logger.info("*** [0.1] Start testing Keycloak userinfo endpoint computed from discovered URL...");
        URL url = KeycloakClientLegacyISFactory.newInstance().computeIntrospectionEndpointURL();
        Assert.assertNotNull(url);
        Assert.assertTrue(url.getProtocol().equals("https"));
        Assert.assertEquals(new URL(DEV_INTROSPECTION_ENDPOINT), url);
        logger.info("Discovered URL is: {}", url);
    }

    @Test
    public void test11QueryOIDCTokenWithDiscoveryInCurrentScope() throws Exception {
        logger.info(
                "*** [1.1] Start testing query OIDC token from Keycloak with endpoint discovery and current scope...");
        oidcTR = KeycloakClientLegacyISFactory.newInstance().queryOIDCToken(CLIENT_ID,
                CLIENT_SECRET);
        logger.info("*** [1.1] OIDC access token: {}", oidcTR.getAccessToken());
        logger.info("*** [1.1] OIDC refresh token: {}", oidcTR.getRefreshToken());
        TestModels.checkTokenResponse(oidcTR);
        TestModels.checkAccessToken(ModelUtils.getAccessTokenFrom(oidcTR),
                "service-account-" + CLIENT_ID, false);
    }

    @Test
    public void test13RefreshOIDCTokenWithDiscovery() throws Exception {
        logger.info("*** [1.3] Start testing refresh OIDC token from Keycloak with endpoint discovery...");
        TokenResponse refreshedTR = KeycloakClientLegacyISFactory.newInstance().refreshToken(
                CLIENT_ID, CLIENT_SECRET,
                oidcTR);
        logger.info("*** [1.3] Refreshed OIDC access token: {}", refreshedTR.getAccessToken());
        logger.info("*** [1.3] Refreshed OIDC refresh token: {}", refreshedTR.getRefreshToken());
        TestModels.checkTokenResponse(refreshedTR);
        TestModels.checkAccessToken(ModelUtils.getAccessTokenFrom(refreshedTR),
                "service-account-" + CLIENT_ID, false);
        TestModels.checkRefreshToken(ModelUtils.getRefreshTokenFrom(refreshedTR));
    }

    @Test
    public void test21QueryUMATokenWithDiscoveryInCurrentScope() throws Exception {
        logger.info(
                "*** [2.1] Start testing query UMA token from Keycloak with endpoint discovery and current scope as audience...");

        umaTR = KeycloakClientLegacyISFactory.newInstance().queryUMAToken(CLIENT_ID,
                CLIENT_SECRET, null);
        logger.info("*** [2.1] UMA access token: {}", umaTR.getAccessToken());
        logger.info("*** [2.1] UMA refresh token: {}", umaTR.getRefreshToken());
        TestModels.checkTokenResponse(umaTR);
        TestModels.checkAccessToken(ModelUtils.getAccessTokenFrom(umaTR),
                "service-account-" + CLIENT_ID, true);
    }

    @Test
    public void test22QueryUMATokenWithDiscovery() throws Exception {
        logger.info("*** [2.2] Start testing query UMA token from Keycloak with endpoint discovery...");
        umaTR = KeycloakClientLegacyISFactory.newInstance().queryUMAToken(CLIENT_ID,
                CLIENT_SECRET, TEST_AUDIENCE,
                null);
        logger.info("*** [2.2] UMA access token: {}", umaTR.getAccessToken());
        logger.info("*** [2.2] UMA refresh token: {}", umaTR.getRefreshToken());
        TestModels.checkTokenResponse(umaTR);
        TestModels.checkAccessToken(ModelUtils.getAccessTokenFrom(umaTR),
                "service-account-" + CLIENT_ID, true);
    }

    @Test
    public void test23QueryUMATokenWithDiscoveryWithOIDCAuthorization() throws Exception {
        logger.info(
                "*** [2.3] Start testing query UMA token from Keycloak with endpoint discovery and OIDC access token for authorization...");

        umaTR = KeycloakClientLegacyISFactory.newInstance().queryUMAToken(oidcTR, TEST_AUDIENCE,
                null);

        logger.info("*** [2.3] UMA access token: {}", umaTR.getAccessToken());
        logger.info("*** [2.3] UMA refresh token: {}", umaTR.getRefreshToken());
        TestModels.checkTokenResponse(umaTR);
        TestModels.checkAccessToken(ModelUtils.getAccessTokenFrom(umaTR),
                "service-account-" + CLIENT_ID, true);
    }

    @Test
    public void test25RefreshUMATokenWithDiscovery() throws Exception {
        logger.info("*** [2.5] Start testing refresh UMA token from Keycloak with endpoint discovery...");
        TokenResponse refreshedTR = KeycloakClientLegacyISFactory.newInstance().refreshToken(
                CLIENT_ID, CLIENT_SECRET,
                umaTR);
        logger.info("*** [2.5] Refreshed UMA access token: {}", refreshedTR.getAccessToken());
        logger.info("*** [2.5] Refreshed UMA refresh token: {}", refreshedTR.getRefreshToken());

        TestModels.checkTokenResponse(refreshedTR);
        TestModels.checkAccessToken(ModelUtils.getAccessTokenFrom(refreshedTR),
                "service-account-" + CLIENT_ID, true);

        TestModels.checkRefreshToken(ModelUtils.getRefreshTokenFrom(refreshedTR));
    }

    @Test(expected = KeycloakClientException.class)
    public void test26RefreshTokenWithDiscoveryAndClientIdFromRefreshToken() throws Exception {
        logger.info("*** [2.6] Start testing refresh UMA token *with error* since is not a public client...");
        KeycloakClientLegacyISFactory.newInstance().refreshToken(umaTR.getRefreshToken());
    }

    @Test
    public void test301IntrospectOIDCAccessTokenWithDiscovery() throws Exception {
        logger.info("*** [3.1] Start testing introspect OIDC access token with endpoint discovery...");
        TokenIntrospectionResponse tir = KeycloakClientLegacyISFactory.newInstance().introspectAccessToken(
                CLIENT_ID,
                CLIENT_SECRET, oidcTR.getAccessToken());

        TestModels.checkTokenIntrospectionResponse(tir);
    }

    @Test
    public void test303IntrospectUMAAccessTokenWithDiscovery() throws Exception {
        logger.info("*** [3.3] Start testing introspect UMA access token with endpoint discovery...");
        TokenIntrospectionResponse tir = KeycloakClientLegacyISFactory.newInstance().introspectAccessToken(
                CLIENT_ID,
                CLIENT_SECRET, umaTR.getAccessToken());

        TestModels.checkTokenIntrospectionResponse(tir);
    }

    @Test
    public void test305OIDCAccessTokenVerificationWithDiscovery() throws Exception {
        logger.info("*** [3.5] Start OIDC access token verification with endpoint discovery...");
        Assert.assertTrue(
                KeycloakClientLegacyISFactory.newInstance().isAccessTokenVerified(CLIENT_ID,
                        CLIENT_SECRET, oidcTR.getAccessToken()));
    }

    @Test
    public void test307OIDCAccessTokenNonVerificationWithDiscovery() throws Exception {
        logger.info("*** [3.7] Start OIDC access token NON verification...");
        Assert.assertFalse(KeycloakClientLegacyISFactory.newInstance().isAccessTokenVerified(
                CLIENT_ID, CLIENT_SECRET,
                OLD_OIDC_ACCESS_TOKEN));
    }

    @Test
    public void test308UMAAccessTokenVerificationWithDiscovery() throws Exception {
        logger.info("*** [3.8] Start UMA access token verification with endpoint discovery...");
        Assert.assertTrue(
                KeycloakClientLegacyISFactory.newInstance().isAccessTokenVerified(CLIENT_ID,
                        CLIENT_SECRET, umaTR.getAccessToken()));
    }

    @Test
    public void test309UMAAccessTokenVerification() throws Exception {
        logger.info("*** [3.9] Start UMA access token verification...");
        Assert.assertTrue(KeycloakClientLegacyISFactory.newInstance().isAccessTokenVerified(
                new URL(DEV_INTROSPECTION_ENDPOINT), CLIENT_ID,
                CLIENT_SECRET, umaTR.getAccessToken()));
    }

    @Test
    public void test310UMAAccessTokenNonVerificationWithDiscovery() throws Exception {
        logger.info("*** [3.10] Start UMA access token NON verification...");
        Assert.assertFalse(KeycloakClientLegacyISFactory.newInstance().isAccessTokenVerified(
                CLIENT_ID, CLIENT_SECRET,
                OLD_UMA_ACCESS_TOKEN));
    }
}
