This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "keycloak-client"

## [v1.0.0]
- First release as refactoring of the `keycloak-client` library; moved the discovery functionality (to be compatible with SGv4) here to provide the backward compatibility. (#23478)
